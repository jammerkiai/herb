-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 30, 2016 at 03:39 PM
-- Server version: 5.5.46-0+deb8u1
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acctg`
--

-- --------------------------------------------------------

--
-- Table structure for table `sales_journal`
--

CREATE TABLE IF NOT EXISTS `sales_journal` (
`id` bigint(20) NOT NULL,
  `postdate` date NOT NULL,
  `dr_code` varchar(10) NOT NULL,
  `cr_code` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `book` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_journal`
--

INSERT INTO `sales_journal` (`id`, `postdate`, `dr_code`, `cr_code`, `amount`, `book`, `created_at`, `updated_at`) VALUES
(29, '2016-01-03', '1001', '4001', 84355.97, 1, '2016-01-30 07:29:20', '0000-00-00 00:00:00'),
(30, '2016-01-03', '1001', '4014', 20.00, 1, '2016-01-30 07:29:20', '0000-00-00 00:00:00'),
(31, '2016-01-03', '1001', '4001', 13413.00, 0, '2016-01-30 07:29:20', '0000-00-00 00:00:00'),
(32, '2016-01-03', '1001', '4014', 10.00, 0, '2016-01-30 07:29:21', '0000-00-00 00:00:00'),
(33, '2016-01-03', '1001', '4024', 740.00, 0, '2016-01-30 07:29:21', '0000-00-00 00:00:00'),
(34, '2016-01-03', '1022', '4001', 40779.87, 1, '2016-01-30 07:29:21', '0000-00-00 00:00:00'),
(35, '2016-01-05', '1001', '4001', 76818.81, 1, '2016-01-30 07:35:41', '0000-00-00 00:00:00'),
(36, '2016-01-05', '1001', '4014', 20.00, 1, '2016-01-30 07:35:41', '0000-00-00 00:00:00'),
(37, '2016-01-05', '1001', '4001', 56672.18, 0, '2016-01-30 07:35:42', '0000-00-00 00:00:00'),
(38, '2016-01-05', '1001', '4021', 150.00, 0, '2016-01-30 07:35:42', '0000-00-00 00:00:00'),
(39, '2016-01-05', '1001', '4024', 1040.00, 0, '2016-01-30 07:35:42', '0000-00-00 00:00:00'),
(40, '2016-01-05', '1001', '4030', 200.00, 0, '2016-01-30 07:35:42', '0000-00-00 00:00:00'),
(41, '2016-01-05', '1022', '4001', 42811.56, 1, '2016-01-30 07:35:42', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales_journal`
--
ALTER TABLE `sales_journal`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales_journal`
--
ALTER TABLE `sales_journal`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
