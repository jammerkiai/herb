<div class="modal fade" id="guestModal" tabindex="-1" role="dialog" aria-labelledby="Guest Search">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guest Search: <span id="foundguests" class="badge">0</span></h4>
      </div>
      <div class="modal-body">
      
      <table class="table table-striped" id="searchguestlist"></table>
      
      </div>
    </div>
  </div>
</div>