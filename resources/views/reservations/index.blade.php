@extends('app')

@section('content')
    
    @include('reservations.cancel')
    @include('reservations.searchguest')

    <div class="row">
        <div class="col-md-6">
            @include('reservations.calendar')
        </div>
        <div class="col-md-6">
            @include('reservations.details')
        </div>
    </div>

    @include('reservations.summary')

    <script>

        var rrooms = {};

        function reloadPageWithParameters(reservecode) {
            var params=[
                'startdate=' + $('#startdate').val(),
                'enddate=' + $('#enddate').val(),
                'room_type_id=' + $('#room_type_id').val(),
                'reserve_code=' + reservecode
            ];
            document.location.href = '{{url("reservations")}}' + '?' + params.join('&');
        }

        $(document).ready(function(){
            $("#clock").jclock({foreground:'yellow',background:'green',fontSize:'20px',timeNotation:'24h'});
            $('.reserve').on('click', function(e){
                e.preventDefault();
                var params=[
                        'startdate=' + $('#startdate').val(),
                        'enddate=' + $('#enddate').val(),
                        'room_type_id=' + $('#room_type_id').val(),
                        'reserve_code=' + $(this).data('reserve')
                    ];
                document.location.href = '{{url("reservations")}}' + '?' + params.join('&');
            });
            $('label.btn').on('click', function(e){
                e.preventDefault();
                var status = $(this).data('status');
                $('.reserve.' + status).toggle();
            });


            $( ".rm-row" ).selectable({
                filter: '.reserve-btn',
                stop: function() {
                    if ($( ".ui-selected", this).length > 0) {
                        var door = {}, days = [];
                        $( ".ui-selected", this ).each(function() {
                            door = $(this).data('door');
                            days.push($(this).data('day'));
                        });
                        rrooms[door] = days;
                    }
                    buildRoomList();
                },
                unselected: function(event, ui) {
                    if (ui !== undefined) {
                        var door = $(ui.unselected).data('door');
                        if (rrooms[door] !== undefined) {
                            delete rrooms[door];
                        }
                    }
                }
            });

            $('#removerooms').on('click', function(e){
                e.preventDefault();
                SelectSelectableElement($('.rm-row'), $('x', $('.rm-row')));
	    });

	    $('#removeaddedrooms').on('click', function(e){
                e.preventDefault();
                SelectSelectableElement($('.rm-row'), $('x', $('.rm-row')));
            });

            function buildRoomList() {
                var rrlist = $('#rroomlist').empty();
                var ctr = 1;
                if (rrooms !== undefined) {
                    $.each(rrooms, function(x, y){
                        if (x !== undefined) {
                            rrlist.append('<tr><td>' + ctr + '</td><td>' + x + '</td><td>' + y[0] + '</td><td>' + y[y.length - 1] + '</td><td></tr>');
                            ctr++;
                        }
                    });
                }
            }

            $('.resform-btn').on('click', function(e){

                var act = $(this).attr('id');

                if (act == 'new') {
                    e.preventDefault();
                    {{--document.location.href='{{url("reservations")}}';--}}
                    reloadPageWithParameters('');
                } else if(act == 'search') {
                    e.preventDefault();
                    {{--document.location.href='{{url("reservations")}}' + '?reserve_code=' + $('#reserve_code').val();--}}
                    reloadPageWithParameters($('#reserve_code').val());

                } else if(act == 'searchguest') {
                    e.preventDefault();
                    $.post(
                        "http://{{ env('FDS_URL') }}/fds/ajax/searchguestbyname.php",
                        {
                            name : $('#reserve_code').val()
                        }
                        ,
                        function (resp) {
                            var data = JSON.parse(resp);
                            $('#searchguestlist').html('');
                            $('#foundguests').html(data.length);
                            for (i=0; i < data.length; i++) {
                                var line = [
                                    '<tr>',
                                    '<td>' + (i + 1) + '</td>',
                                    '<td>' + data[i]['guestname'] + '</td>',
                                    '<td><a href="?reserve_code=' + data[i]['reserve_code'] + '">' + data[i]['reserve_code'] + '</a></td>',
                                    '<td>' + data[i]['date_created'] + '</td>',
                                    '<td>' + data[i]['status'] + '</td>',
                                    '</tr>'
                                ];
                                $('#searchguestlist').append(line.join());
                            }
                        }
                    );

                } else if(act == 'save') {
                    e.preventDefault();
                    if ( validate() ) {
                        $('#act').val('save');
                        $('#rrooms').val( JSON.stringify(rrooms) );
                        $('#reserveform').submit();
                    }
                    // if ($('#guest_id').val() == '') {
                    //     alert('Please enter a valid guest');
                    // } else {
                    //     $('#act').val('save');
                    //     $('#rrooms').val( JSON.stringify(rrooms) );
                    //     $('#reserveform').submit();
                    // }
                } else if(act == 'cancel') {
                    e.preventDefault();
                    $('#act').val('cancel');
                    $('#rrooms').val( JSON.stringify(rrooms) );
                    $('#reservecode').val( $('#reserve_code').val() );
                    $('#bookingnumber').val( $('#booking_number').val() );
                    $('#partnername').val( $('#partner').val() );
                    $('#cancelform').submit();
                } else if (act == 'print') {
                    e.preventDefault();
                    $.post(
                        'http://{{ env('FDS_URL') }}/fds/ajax/printreserve.php',
                        {
                            rescode: $('#reserve_code').val()
                        },
                        function(resp){
                            console.log(resp);
                        });
                }
            });

            $('.checkin-icon').on('click', function(e){
                e.preventDefault();
                var act = $(this).data('act');
                if (act == 'checkin') {
                    var params = [
                        'rrid=' + $(this).data('rrid'),
                        'code=' + $('#reserve_code').val(),
                        'fn=' + $('#firstname').val(),
                        'ln=' + $('#lastname').val(),
                        'room=' + $(this).data('roomid'),
                        'dep=' + $('#reserve_fee').val()
                    ];
                    window.location.href='http://{{ env('FDS_URL') }}/fds/ajax/checkinform.php?' + params.join('&');
                } else if (act == 'checkinclose') {
                    var params = [
                        'close=1',
                        'rrid=' + $(this).data('rrid'),
                        'code=' + $('#reserve_code').val(),
                        'fn=' + $('#firstname').val(),
                        'ln=' + $('#lastname').val(),
                        'room=' + $(this).data('roomid'),
                        'dep=' + $('#reserve_fee').val()
                    ];
                    window.location.href='http://{{ env('FDS_URL') }}/fds/ajax/checkinform.php?' + params.join('&');
                } else if (act == 'rrdel') {
                    var url = '{{url('reservations/removeroom')}}/' + $(this).data('rrid');
                    $.post(url, {}, function(resp){
                        document.location.href = '{{url('reservations')}}?reserve_code=' + $('#reserve_code').val();
                    });
                }
            });

            $('.paymentType').on('click', function(e){
                $('.paymentTypeDetails').hide();

                if ($(this).val() == 'Bank') {
                    $('#bank_details').show();
                } else if ($(this).val() == 'Card') {
                    $('#card_details').show();
                } 
            });

            $('#cardselect').on('click', function(){
                if ($(this).is(':checked')) {
                    $('#cancelCardDetails').show(); 
                } else {
                    $('#cancelCardDetails').hide(); 
                }
            });
        });

        function SelectSelectableElement(selectableContainer, elementsToSelect) {
            $.each(selectableContainer, function(i, j) {
                var selectableContainer = $(j);
                $(".ui-selected", selectableContainer).not(elementsToSelect).removeClass("ui-selected").addClass("ui-unselecting");
                $(elementsToSelect).not(".ui-selected").addClass("ui-selecting");
                selectableContainer.data('ui-selectable')._mouseStop();
            });
        }

        function validate() {
            var required = [
                'pax', 'reserve_date', 'firstname', 'lastname', 'reserve_fee', 'mode'
            ];
            var passed = true;

            $('.error').removeClass('error');

            $.each(required, function(idx, param){
                if ($('#' + param).val() == '') {
                    $('#' + param).addClass('error');
                    passed = false;
                }
            });

            if ($('#mode').val() == 'Partner') {
                $.each(['promo_code', 'booking_number', 'partner'], function(idx, param){
                    if ($('#' + param).val() == '') {
                        $('#' + param).addClass('error');
                        passed = false;
                    }
                });
            }

            if ( $('input[name=payment_type]:checked').val()==undefined ) {
                $('.paymentType').parent('label').addClass('error');
                passed = false;
            }

            if (!passed) {

                alert('Errors found. Please review your input');
            }

            return passed;
        }


    </script>
@endsection
