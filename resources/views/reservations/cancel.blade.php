<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="Cancel Reservation">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cancel Reservation</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" id="cancelform" name="cancelform" action="{{url('reservations/cancel')}}">
            <div class="form-group">
              <label for="cancelReason" class="col-sm-3 control-label">Reason</label>
              <div class="col-sm-9">
              <select id="cancelReason" class="form-control" name="cancelReason">
                  <option value="No Show">No Show</option>
                  <option value="Cancelled">Cancelled</option>
              </select>
              </div>
            </div>
            <div class="form-group">
              <label for="cancelPenalty" class="col-sm-3 control-label">Penalty</label>
              <div class="col-sm-9">
              <input type="number" id="cancelPenalty" name="cancelPenalty" class="form-control" />
              </div>
            </div>
            <div class="form-group">
                <label for="cancelRemarks" class="col-sm-3 control-label">Remarks</label>
                <div class="col-sm-9">
                <textarea name="cancelRemarks" id="cancelRemarks" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="cancelRemarks" class="col-sm-offset-3 col-sm-9control-label">
                <input type="checkbox" class="checkbox-inline" id="cardselect" name="cardselect" value="1" /> Pay by Card
                </label>
            </div>
            <div class="panel panel-default" id="cancelCardDetails" style="display: none;">
              <div class="panel-heading">
                Card Details
              </div>
              <div class="panel-body">
                <div class="form-group">
                    <label for="cardType" class="col-sm-3 control-label">Card Type</label>
                    <div class="col-sm-9">
                    <select id="cardType" class="form-control" name="cardType">
                        <option value="AMEX">AMEX</option>
                        <option value="JBC">JBC</option>
                        <option value="Visa">Visa</option>
                        <option value="Mastercard">MasterCard</option>
                        <option value="BDO Card">BDO Card</option>
                        <option value="Express Net">ExpressNet</option>
                        <option value="Megalink">Megalink</option>
                        <option value="BancNet">BancNet</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="approvalNumber" class="col-sm-3 control-label">Approval Number</label>
                    <div class="col-sm-9">
                    <input type="text" id="approvalNumber" name="approvalNumber" class="form-control" >
                    </div>
                </div> 
                <div class="form-group">
                    <label for="batchNumber" class="col-sm-3 control-label">Batch Number</label>
                    <div class="col-sm-9">
                    <input type="text" id="batchNumber" name="batchNumber" class="form-control" >
                    </div>
                </div>  
              </div>
            </div>
            <input type="hidden" name="reservecode" id="reservecode" />
            <input type="hidden" name="bookingnumber" id="bookingnumber" />
            <input type="hidden" name="partnername" id="partnername" />
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary resform-btn" id="cancel" >Submit</button>
      </div>
    </div>
  </div>
</div>