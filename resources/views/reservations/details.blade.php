<form class="form form-horizontal" method="post" id="reserveform" name="reserveform" action="{{url('reservations/save')}}">
    
    <div class="panel @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) panel-danger @else panel-primary @endif reserve-details">
        <div class="panel-heading form-inline">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Search</span>
                    <input type="text" name="reserve_code" class="form-control" 
                        id="reserve_code" placeholder="Code or Guest"
                        value="{{$reservation->reserve_code or ''}}" />
                    <span class="input-group-btn">
                        <button type="button" class="resform-btn btn btn-default" id="search"><span class="glyphicon glyphicon-search"></span></button>
                        <button type="button" class="resform-btn btn btn-default" 
                        data-toggle="modal" data-target="#guestModal" 
                        id="searchguest"><span class="glyphicon glyphicon-user"></span></button>
                    </span>
                </div>
                <button class="btn btn-default btn-sm resform-btn" id="new" type="button">New</button>
                <button class="btn btn-default btn-sm resform-btn" id="save" type="button"
                @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) disabled @endif>Save</button>
                <button class="btn btn-default btn-sm resform-btn" id="print" type="button">Print</button>
                <button class="btn btn-default btn-sm resform-btn" 
                data-toggle="modal" data-target="#cancelModal" type="button"
                @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) disabled @endif>Cancel</button>
                <input type="hidden" name="rrooms" id="rrooms" />
                <input type="hidden" name="act" id="act" />
                <input type="hidden" name="uid" value="{{ $uid }}">
        </div>
        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            Reserved Rooms
                            <button class="btn btn-xs pull-right" id="removerooms" type="button" 
                                data-toggle="tooltip" title="Remove room from this reservation">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </div>
                        <table class="table">
                            <thead>
                            <tr><th>#</th><th>Room Type</th><th>Room</th><th>In</th><th>Out</th><th>Action</th></tr>
                            </thead>
                            <tbody>
                            @foreach($reservation->reserveRooms as $reserveRoom)
                                <tr>
                                    <td><input type="checkbox" name="current_reserve_room" value="{{$reserveRoom->rr_id}}"></td>
                                    <td>{{$reserveRoom->roomType->room_type_name}}</td>
                                    <td>{{$reserveRoom->room->door_name}}</td>
                                    <td>{{$reserveRoom->checkin}}</td>
                                    <td>{{$reserveRoom->checkout}}</td>
                                    <td>
                                        <button data-act="checkin" data-rrid="{{$reserveRoom->rr_id}}"
                                                data-toggle="tooltip" title="Check-in without closing this reservation"
                                                data-placement="left"
                                                data-roomid="{{$reserveRoom->room_id}}"
                                                type="button" class="checkin-icon btn btn-xs checkin"
                                                @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) disabled @endif>
                                            <span class="glyphicon glyphicon-log-in"></span>
                                        </button>
                                        <button data-act="checkinclose" data-rrid="{{$reserveRoom->rr_id}}"
                                                data-toggle="tooltip" title="Check-in and close this reservation"
                                                data-placement="top"
                                                data-roomid="{{$reserveRoom->room_id}}"
                                                type="button" class="checkin-icon btn btn-xs checkinclose"
                                                @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) disabled @endif>
                                            <span class="glyphicon glyphicon-ok-circle"></span>
                                        </button>
                                        <button data-act="rrdel" data-rrid="{{$reserveRoom->rr_id}}" 
                                                data-toggle="tooltip" title="Remove this room from this reservation"
                                                data-placement="bottom"
                                                type="button" class="checkin-icon btn btn-xs rrdel"
                                                @if(in_array($reservation->status, ['Cancelled', 'No Show', 'Claimed'])) disabled @endif>
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6"> 
                    @include('reservations.reserverooms')
                    @include('reservations.particulars')
                    @include('reservations.partnerinfo')

                </div>
                <div class="col-md-6">
                    
                    @include('reservations.guestinfo')
                    @include('reservations.deposit')
                </div>
            </div>
        </div>
    </div>
</form>
<script>

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});    

</script>
