<?php

namespace App\Http\Controllers\Reservations;

use App\Libraries\Repositories\ReserveRoomRepository;
use App\Models\BookingRoomTypes;
use App\Models\Calendar;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\Partner;
use App\Models\PartnerTransaction;
use App\Models\Reservation;
use App\Models\ReserveRoom;
use App\Models\ReservationTransactions;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ReserveRoomRepository $reserveRoomRepo)
    {

        $roomTypes = BookingRoomTypes::all();
        $firstRoomType = $roomTypes->last();
        $uid = $request->get('uid');
        $now = new \DateTime('now');
        $startdate = $request->get('startdate', $now->format('Y-m-d'));
        $enddate = $request->get('enddate', $now->add(new \DateInterval('P10D'))->format('Y-m-d'));
        $room_type_id = $request->get('room_type_id', $firstRoomType->room_type_id);
        
        $reservation = new Reservation();

        if ($request->has('reserve_code')) {
            $reserve_code = $request->get('reserve_code');
            $reservation = Reservation::where('reserve_code', $reserve_code)->get()->first();

            if (is_null($reservation)) {
                $reservation = new Reservation();
                return redirect('reservations')->with(['status' => 'Reserve code not found.']);
            }
        }  

        $calendar = $reserveRoomRepo->findReserveRoomsByRangeAndRoomType($startdate, $enddate, $room_type_id);

        $dates = Calendar::getInclusiveDates($startdate, $enddate);
        $partners = Partner::all();
        $modes = ['Phone', 'Email', 'Partner'];

        $request->flash();

        $cardTypes = ['AMEX', 'JBC', 'Visa', 'Mastercard', 'BDO Card', 'Express Net', 'Megalink', 'BancNet', 'BPI'];
        return view('reservations.index', compact('calendar', 'roomTypes',
            'dates', 'startdate', 'enddate', 'reservation', 'partners', 'cardTypes', 'modes', 'uid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $all = $request->all();

        if ($all['act'] == 'cancel') {
            $result = $this->cancelReservation($all);  
        } else {
            $result = $this->saveReservation($all);
        }

        return redirect('reservations' . '?uid=' . $all['uid'] . '&reserve_code=' . $result['reservecode'])->with('status', $result['message']);
        
    }

    public function cancel(Request $request) {
        $all = $request->all();
        $result = $this->cancelReservation($request->all());
        return redirect('reservations' . '?uid=' . $all['uid'] . '&reserve_code=' . $result['reservecode'])->with('status', $result['message']);
    }

    protected function saveReservation($all) {
        $rooms = json_decode($all['rrooms'], true);

        $now = date('Y-m-d H:i:s');
        $reservecode = (isset($all['reserve_code']) && trim($all['reserve_code']) != '') ? $all['reserve_code'] : Reservation::generateReserveCode();

        $reservation = [
            "guest_id" => $all['guest_id'],
            "reserve_date" => $all['reserve_date'],
            "Partner" => $all['partner'],
            "pax" => $all['pax'],
            "pickup_time" => $all['pickup_time'],
            "pickup_location" => $all['pickup_location'],
            "payment_type" => (isset($all['payment_type'])) ? $all['payment_type'] : '',
            "notes" => $all['notes'],
            "reserve_fee" => $all['reserve_fee'],
            "card_type" => $all['cardType'],
            // "date_created" => $now,
            // "date_updated" => $now,
            "batch_number" => $all['batch_number'],
            "is_debit" => isset($all['is_debit']) ? 1 : 0,
            "card_suffix" => $all['card_suffix'],
            "multi_entry_approver" => "",
            "approval_code" => isset($all['approval_code'])?$all['approval_code']:'',
            "bank_code" => $all['bank_code'],
            "mode" => $all['mode'],
            "terminal" => isset($all['terminal']) ? $all['terminal'] : ''
        ];


        $res = Reservation::find($reservecode);
        if (is_null($res)) {
            $reservation['reserve_code'] = $reservecode;
            $reservation['date_created'] = $now;
            Reservation::create($reservation);
        } else {
            $oldReserveData = Reservation::find($reservecode);
            $reservation['date_updated'] = $now;
            Reservation::find($reservecode)->update($reservation);
        }

        if ($all['mode'] !== 'Partner') {
            $booking_number = $reservecode;
        } else {
            $booking_number = isset($all['booking_number']) ? $all['booking_number'] : $reservecode;
        }

        $partnerTrxn = [
            "booking_number" => $booking_number,
            "payable" => isset($all['commission']) ? $all['commission'] : 0,
            "transaction_date" => isset($all['reserve_date'])? $all['reserve_date'] : $now,
            "reserve_code" => $reservecode,
            "result_status" => 'Confirmed',
            "receivable" => '0.00',
            "partner_name" => isset($all['partner']) ? $all['partner'] : '',
            "remarks" => isset($all['remarks']) ? $all['remarks'] : '',
            "promo_code" => isset($all['promo_code']) ? $all['promo_code'] : ''
        ];


        $pt = PartnerTransaction::where(['reserve_code' => $reservecode])->get()->first();
        if (is_null($pt)) {
            PartnerTransaction::create($partnerTrxn);
        } else {
            $pt->update($partnerTrxn);
        }

        $reservationTxn = [
            "transaction_date" => $now,
            "reservation_code" => $reservecode,
            "occupancy_id" => 0,
            "amount_deposit" => $all['reserve_fee'],
            "amount_claimed" => 0,
            "update_by" => $all['uid'],
            "remarks" => ''
        ];

        $rt = ReservationTransactions::where(['reservation_code' => $reservecode])->get()->first();
        if (is_null($rt)) {
            ReservationTransactions::create($reservationTxn);
            if (isset($all['payment_type']) && $all['payment_type'] == 'Cash') {
                $this->updateCurrentCash($all['reserve_fee'], 'in', $all['uid']);
            }
        } else {

            if (isset($all['payment_type']) && $all['payment_type'] == 'Cash') {
                
                if ($rt->amount_deposit > $all['reserve_fee']) {
                    //reserve fee has been increased, cash needs to be placed in system
                    $amt = $rt->amount_deposit - $all['reserve_fee'];

                    $this->updateCurrentCash($amt, 'out', $all['uid']);

                } elseif ($rt->amount_deposit < $all['reserve_fee']) {
                    //cash removed from current_cash
                    $amt = $all['reserve_fee'] - $rt->amount_deposit;

                    $this->updateCurrentCash($amt, 'in', $all['uid']);
                }
            }

            $rt->update($reservationTxn);


        }

        if (!is_null($rooms)) {
            $this->saveReserveRooms($rooms, $reservecode);
        } 

        return ['reservecode'=>$reservecode, 'message' => 'Reservation data saved'];
    }

    private function updateCurrentCash($amt, $type, $by) {
        
        $this->externalRequest(
            'http://' . env('FDS_URL') . '/fds/ajax/updatecurrentcash.php', 
            'POST', 
            [
                'amt' => $amt,
                'type' => $type,
                'by' => $by
            ]
        );
    }

    protected function cancelReservation($all) {
        
        $reservecode = $all['reservecode'];
        // Reservation::where(['reserve_code' => $reservecode])->get()->first()->update(['status' => 'Cancelled']);
        // $rooms = ReserveRoom::where(['reserve_code' => $reservecode])->get()->all();

        // foreach ($rooms as $r) {
        //     ReserveRoom::find($r->rr_id)->update(['status' => 'Cancelled']);
        // }

        $postdata = [
            'code' => $all['reservecode'],
            'penalty' => $all['cancelPenalty'],
            'reason' => $all['cancelReason'],
            'remark' => $all['cancelRemarks'],
            'bookingnum' => $all['bookingnumber'],
            'approval' => $all['approvalNumber'],
            'batchnum' => $all['batchNumber'],
            'cardtype' => $all['cardType'],
            'cardselect' => isset($all['cardselect']) ? 1 : 0,
            'partner' => $all['partnername']
        ];

        $this->externalRequest(
            'http://' . env('FDS_URL') . '/fds/ajax/cancelreservation.php', 
            'POST', 
            $postdata
        );

        return ['reservecode' => $reservecode, 'message' => 'Reservation cancelled'
        ];
    }

    protected function saveReserveRooms($rrooms, $reservecode) {
        foreach ($rrooms as $door => $dates) {
            $rm  = Room::where(['door_name' => $door])->get()->first();

            $resKey = [
                'reserve_code' => $reservecode,
                'room_type_id' => $rm->room_type_id,
                'room_id' => $rm->room_id
            ];

            $reserveRoomModel = ReserveRoom::firstOrNew($resKey);
            $reserveRoomModel->checkin = $dates[0];
            $reserveRoomModel->checkout = $dates[ count($dates) - 1 ];
            $reserveRoomModel->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function guest(Request $request)
    {
        $fn = $request->get('firstname');
        $ln = $request->get('lastname');

        $guest = Guest::firstOrCreate(['firstname' => $fn, 'lastname' => $ln]);

        return response()->json($guest);

    }

    public function findguest(Request $request) {

    }

    public function removeRoom($id)
    {
        ReserveRoom::destroy($id);

    }

    public function report()
    {
        
    }

    public function externalRequest($url, $method, $data) {
        $opts = array('http' =>
            array(
                'method'  => $method,
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents( $url , false, $context);
        return $result;
    }

}


