<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class ReservationTransactions extends Model
{
    
	public $table = "reservation_transactions";
    public $primaryKey = 'transaction_id';

	public $fillable = [
	    "transaction_id",
		"transaction_date",
		"reservation_code",
		"occupancy_id",
		"amount_deposit",
		"amount_claimed",
		"update_by",
		"remarks"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
       "transaction_id" => 'integer',
		"transaction_date" => 'datetime',
		"reservation_code" => 'string',
		"occupancy_id" => 'integer',
		"amount_deposit" => 'decimal',
		"amount_claimed" => 'decimal',
		"update_by" => 'integer',
		"remarks" => 'text'
    ];

	public static $rules = [
	    
	];

	public function reservation() {
		return $this->belongsTo('App\Models\Reservation', 'reservation_code', 'reserve_code');
	}

}